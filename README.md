Devices Gateway
===============

Build: [![CircleCI](https://circleci.com/bb/challengerdevs/tcpgateway.svg?style=svg)](https://circleci.com/bb/challengerdevs/gpsdriver)

# Requirements

- go 1.10
- [librdkafka](https://github.com/edenhill/librdkafka)